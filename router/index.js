import { Router } from "express";

const router = Router();



router.post('/', (req, res) => {
    const params = {
        numero: req.body.numero,
        nombre: req.body.nombre,
        adress: req.body.adress,
        nivel: req.body.nivel,
        pago: req.body.pago,
        horas: req.body.horas,
        hijos: req.body.hijos,
    }
    res.render('index', params);
})

router.get('/', (req, res) => {
    const params = {
        numero: req.query.numero,
        nombre: req.query.nombre,
        adress: req.query.adress,
        nivel: req.query.nivel,
        pago: req.query.pago,
        horas: req.query.horas,
        hijos: req.query.hijos,
    }

    res.render('index', params)
})

export default router;